var express = require('express');
var app = express();

var port = parseInt(process.argv[2]) || 3000

app.use(express.static(__dirname + "/public"))

app.listen(port, function(rep,require){
    console.log("port started on %d" , port);
});